#pragma once

#include <Zodiac.grpc.pb.h>
#include "ZodiacHelper.h"

class CZodiacServiceImpl final : public zodiac::Zodiac::Service {

public: 
	CZodiacServiceImpl() {}

	::grpc::Status GetZodiac(::grpc::ServerContext* context, const zodiac::ZodiacRequest* request,
		zodiac::ZodiacReply* reply) override;
};

