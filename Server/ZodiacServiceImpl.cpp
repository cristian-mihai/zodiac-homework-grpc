#include "ZodiacServiceImpl.h"

::grpc::Status CZodiacServiceImpl::GetZodiac(::grpc::ServerContext* context, const zodiac::ZodiacRequest* request, 
	zodiac::ZodiacReply* reply)
{
	auto birthDate = ZodiacHelper::SplitString(request->date(), "/");
	
	for (auto data : ZodiacHelper::Data) {

		auto startDate = std::get<1>(data) + "/" + birthDate[2];
		auto endDate = std::get<2>(data) + "/" + birthDate[2];
		
		if (ZodiacHelper::IsDateInRange(request->date(), startDate, endDate)) {
			reply->set_message("Your star sign is: " + std::get<0>(data));
			break;
		}	
	}

	return ::grpc::Status::OK;
}
