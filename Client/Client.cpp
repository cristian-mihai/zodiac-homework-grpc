#include <iostream>
#include "Zodiac.grpc.pb.h"
#include <ZodiacHelper.h>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;
using grpc::Status;

using zodiac::Zodiac;
using zodiac::ZodiacRequest;
using zodiac::ZodiacReply;

int main()
{
	grpc_init();
	ClientContext context;

	auto zodiac_stub = Zodiac::NewStub(grpc::CreateChannel("localhost:8888",
		grpc::InsecureChannelCredentials()));

	ZodiacRequest request;
	ZodiacReply reply;

	std::string date;
	std::cout << "Introduceti data dorita de la tastatura (MM/DD/YYYY): ";
	std::cin >> date;

	if (!ZodiacHelper::CheckDate(date)) {
		std::cout << "Data introdusa este invalida! \n";
		return 1;
	}

	request.set_date(date);
	auto status = zodiac_stub->GetZodiac(&context, request, &reply);

	if (status.ok()) {
		std::cout << reply.message() << "\n";
	}
}
