#include "ZodiacHelper.h"

std::vector<std::tuple<std::string, std::string, std::string>> ZodiacHelper::Data;

std::vector<std::string> ZodiacHelper::SplitString(const std::string& str,
    const std::string& delim)
{
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;

    do {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos - prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    } while (pos < str.length() && prev < str.length());

    return tokens;
}

void ZodiacHelper::ParseData(const std::string& fileName)
{
    std::ifstream input(fileName);

    for (std::string line; getline(input, line);) {

        auto str = SplitString(line, "|");
        Data.push_back(std::make_tuple(str[0], str[1], str[2]));
    }
}

bool ZodiacHelper::IsLeapYear(std::string year)
{
    if (((std::stoi(year) % 4 == 0) && (std::stoi(year) % 100 != 0)) || (std::stoi(year) % 400 == 0))
        return true;
    return false;
}

bool ZodiacHelper::CheckDate(const std::string& date)
{
    const std::regex date_regex( R"(^\s*(\d\d)/(\d\d)/(\d{4}))" );
    std::smatch match;

    if (std::regex_match(date, match, date_regex)) {

        auto split = SplitString(date, "/");

        // months
        if (std::stoi(split[0]) > 12)
            return false;

        // leap year 
        if (!IsLeapYear(split[2]) && split[0] == "2" && std::stoi(split[1]) > 28)
            return false;

        // 31 days
        if (std::stoi(split[0]) % 2 != 0 && std::stoi(split[1]) > 31)
            return false;

        // 30 days
        if (std::stoi(split[0]) % 2 == 0 && std::stoi(split[1]) > 30)
            return false;
    }
    else
        return false;

    return true;
}

bool ZodiacHelper::IsDateInRange(const std::string& birthDate, const std::string& startDate,
    const std::string& endDate)
{
    auto birthDateSplit = SplitString(birthDate, "/");
    auto startDateSplit = SplitString(startDate, "/");
    auto endDateSplit = SplitString(endDate, "/");

    int iDate = (std::stoi(birthDateSplit[2]) * 10000) + (std::stoi(birthDateSplit[0]) * 100) + std::stoi(birthDateSplit[1]);
    int iStartDate = (std::stoi(startDateSplit[2]) * 10000) + (std::stoi(startDateSplit[0]) * 100) + std::stoi(startDateSplit[1]);
    int iEndDate = (std::stoi(endDateSplit[2]) * 10000) + (std::stoi(endDateSplit[0]) * 100) + std::stoi(endDateSplit[1]);

    if (iDate >= iStartDate && iDate <= iEndDate)
        return true;
    return false;
}

void ZodiacHelper::print()
{
    for (auto d : Data) {
        std::cout << std::get<0>(d) << " " << std::get<1>(d) << " " << std::get<2>(d) << "\n";
    }
}
