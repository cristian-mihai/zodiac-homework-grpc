#pragma once
#ifndef HELPER_H
#define HELPER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <tuple>
#include <string>
#include <regex>

namespace ZodiacHelper {

	extern std::vector<std::tuple<std::string, std::string, std::string>> Data;
	std::vector<std::string> SplitString(const std::string& str, const std::string& delim);
	void ParseData(const std::string& fileName);
	bool IsLeapYear(std::string year);
	bool CheckDate(const std::string& date);
	bool IsDateInRange(const std::string& birthDate, const std::string& startDate,
		const std::string& endDate);

	void print();
};

#endif